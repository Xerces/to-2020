/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <math.h>
#define PI (double)3.141592
#define DT (double)0.005
#define DECMAX (int)1000
#define ACCMAX (int)600
#define VMAX (int)500
#define ABS(X) X>0?X:-X
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
//yo

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
float wheelDiam = 51.12,entreaxe = 220;

uint16_t resolution = 2048; // parce que j'ai l'impréssion d'avoir 4 fois plus de tics
int sens = 1;



  double modulo(double x, double y)
  {
  float reste;
   int resultat = floor (x / y);
   return reste = x - (resultat * y);

  }
    
          

 void Displacementdt(double *Prodt,double *Pthetadt,double *Px,double *Py,double *Ptheta,double *Pro)
{
  
  static uint32_t encostock1=2147483647;
  static uint32_t encostock2=2147483647;
  double multiplicateur=((wheelDiam*PI)/resolution);
  double wheelCoef = 1/1.00052;
  //printf("enco1 dans la fonction : %lu \n",encostock1);
  //printf("enco2 dans la fonction : %lu \n",encostock2);

  *Prodt=-(((multiplicateur/2)*((double)-TIM2->CNT+(double)wheelCoef*TIM5->CNT))-(multiplicateur/2*((double)encostock1+(double)wheelCoef*encostock2)));
  *Pthetadt=-(((multiplicateur/entreaxe)*((double)-TIM2->CNT-(double)wheelCoef*TIM5->CNT))-((multiplicateur/entreaxe)*((double)encostock1-(double)wheelCoef*encostock2)));
  
  encostock1=-TIM2->CNT;
  encostock2=TIM5->CNT;
  //printf("codeur1 codeur2 : %lf / %lf \n",(double)encostock1,(double)encostock2);
    if (sens==1)
  {
    *Ptheta=*Ptheta+*Pthetadt; 
    //printf("sens = 1\n");
  }
  else
  {
    *Ptheta=*Ptheta-*Pthetadt; 
    //printf("sens = 0\n");
  }
  
  *Px=*Px+*Prodt*cos(*Ptheta);
  *Py=*Py+*Prodt*sin(*Ptheta);
  *Pro=*Pro+*Prodt;
}

double arg(double x,double y)
{
  if(x>0)
  {
    return atan2(y,x);
  }
  else if(x==0)
  {
    if(y>0)
    {
      return PI/2;
    }
    else if(y<0)
    {
      return -PI/2;
    }
    else
    {
      return 0.0; // absurd 
    }
  }
  else
  {
    return PI-atan2(y,x);
  }
}

void ErreurPolaire(double *Pdist,double *Pcap,double *Perreurtheta, double *Px,double *Py,double *Ptheta,double *Pxcons,double *Pycons,double *Pthetacons)
{
  double erreurx=0, erreury=0;
  *Perreurtheta=*Pthetacons-*Ptheta;
  erreurx=*Pxcons-*Px;
  erreury=*Pycons-*Py;

  //*Pcap=((atan2(erreurx,erreury)-*Ptheta)-((int)atan2(erreurx,erreury)-(int)*Ptheta)/(PI));
  *Pcap=arg(erreurx,erreury)-*Ptheta;
  *Pdist=sqrt((erreurx*erreurx)+(erreury*erreury));
  
  if (*Pcap<-PI/2 || *Pcap>PI/2)
  {
    *Pdist=-*Pdist;
    //*Pcap=(*Pcap+PI)-((int)*Pcap+(int)PI)/(PI);
    *Pcap=modulo(*Pcap+PI,PI);
    //printf("dist= %lf\tcap= %lf\n",*Pdist,*Pcap*180/PI);
  }
  
  
  
}

void RampeTranslation(double *Pdist, double *PVdist,double *PvitesseRob,double *Pdt)
{
  static double D2=0,D1=0;
  *PvitesseRob = (D2-D1)/(*Pdt);
  double distanceFreinage = (*PvitesseRob**PvitesseRob)/(2*DECMAX);
  if(*Pdist<distanceFreinage)
  {
    *PVdist=*PvitesseRob-(DECMAX*(*Pdt));
  }
  else
  {
  if(*PvitesseRob<VMAX)
  {
    *PVdist=*PvitesseRob+(ACCMAX*(*Pdt));
  }
  else
  {
    *PVdist=VMAX;
  }    
  }
  D2=D1;
  D1=*Pdist;
}
void RampeRotation(double *Pcap, double *PVcap,double *PomegaRob,double *Pdt)
{
  static double A2=0,A1=0;
  *PomegaRob = (A1-A2)/(*Pdt);
  double angleFreinage = (*PomegaRob**PomegaRob)/(2*DECMAX);
  if(*Pcap<angleFreinage)
  {
    *PVcap=*PomegaRob-(DECMAX*(*Pdt));
  }
  else
  {
    if(*PomegaRob<VMAX)
    {
      *PVcap=*PomegaRob+(ACCMAX*(*Pdt));
    }
    else
    {
      *PVcap=VMAX;
    }    
  }
  A2=A1;
  A1=*Pcap;
}

double PIDdist(double *PVdist)
{
static double PIDD2=0,PIDD1=0;
double proDist=1,derDist=0,intDist=0,targetDistance=0;

targetDistance=proDist*PIDD1+(PIDD2+PIDD1)*intDist+(PIDD1-PIDD2)*derDist;

  PIDD2=PIDD1;
  PIDD1=*PVdist;
  return targetDistance;
}
double PIDcap(double *PVcap)
{
static double PIDA2=0,PIDA1=0;
double proCap=1,derCap=0,intCap=0, targetAngle=0;

targetAngle=proCap*PIDA1+(PIDA2+PIDA1)*intCap+(PIDA1-PIDA2)*derCap;

  PIDA2=PIDA1;
  PIDA1=*PVcap;
  return targetAngle;
}

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  double x=0.0,y=0.0,theta=0.0,rho=0.0;
  double rhodt=0.0,thetadt=0.0 ,dist = 0, cap=0,xcons=1000,ycons=0,thetacons=0*PI/180, erreurtheta=0, Vdist=0, Vcap=0;
  double vitesseRob=0,omegaRob=0,targetDist=0,targetAngle=0,dt=0;
  uint16_t c=0;
  // Mon commentaire
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM5_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1); 
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);

  // démarrage des timers nécéssaire au fonctionnement des codeurs
  HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_1);
  HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_2);
  HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_1);
  HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint16_t PWM_1= 25, PWM_2= 25;

  while (1)
  {
    
    //printf("rho main  %lf",rho);
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);

    HAL_Delay(1000*DT);
    //dt=HAL_GetTick()-dt;
   

    //encoder1 = TIM2->CNT;
    //encoder2 = TIM5->CNT;

    Displacementdt(&rhodt,&thetadt,&x,&y,&theta, &rho);
    ErreurPolaire(&dist,&cap,&erreurtheta,&x,&y,&theta,&xcons,&ycons,&thetacons);
    RampeTranslation(&dist,&Vdist,&vitesseRob,&dt);
    RampeRotation(&cap,&Vcap,&omegaRob,&dt);
    targetDist=PIDdist(&Vdist);
    targetAngle=PIDcap(&Vcap);

    PWM_1 = targetDist + targetAngle;
    PWM_2 = targetDist - targetAngle;
    TIM3->CCR1 = 0;//avance gauche
    TIM3->CCR2 = 0;//avance droite
    TIM3->CCR3 = 0;//recule gauche
    TIM3->CCR4 = 25;
    /*HAL_Delay(1000);
    htim3.Instance->CCR1 = 0;
    htim3.Instance->CCR2 = 0;
    HAL_Delay(1000);*/

    /*htim3.Instance->CCR3 = 0;
    htim3.Instance->CCR4 = 0;  
    htim3.Instance->CCR1 = PWM_1;
    htim3.Instance->CCR2 = PWM_2;
    printf("sens 1");
    HAL_Delay(1000);
    htim3.Instance->CCR1 = 0;
    htim3.Instance->CCR2 = 0;
    htim3.Instance->CCR3 = PWM_1;
    htim3.Instance->CCR4 = PWM_2;
    printf("sens 2");
    HAL_Delay(1000);*/
    
    if (c==5)
    {
      //printf("x=%lf\ty=%lf\ttheta=%3.2lf\n",x,y,theta*180/PI);
      //printf("CNT1:%lu\tCNT2:%lu\n",TIM2->CNT-2147483647,-TIM5->CNT-2147483647);

      //printf("dist= %lf\tcap= %lf\n",dist,cap*180/PI);

      /*printf("rho main et rhodt %lf,%lf\n",rho,rhodt);
      printf("theta main et thetadt %lf,%lf\n",theta,thetadt); */
      printf("1 et 2 :%i /%i\n",PWM_1,PWM_2);
      //printf("les tragets : %lf / %lf \n",targetDist,targetAngle);
      c=0;
    }
    else
    {
      c++;
    }
    

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 360;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
